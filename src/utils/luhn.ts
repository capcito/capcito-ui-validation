/* eslint-disable no-unused-vars */
interface Options {
  min?: number,
  max?: number,
}

export interface Response {
  isValid: boolean,
  details?: any,
  error?: {
    name: string,
    message: string
  },
}

export enum Errors {
  TOO_SHORT = 'TOO_SHORT',
  TOO_LONG = 'TOO_LONG',
  INVALID_CHECKSUM = 'INVALID_CHECKSUM',
  INVALID_CHARACTERS = 'INVALID_CHARACTERS',
}

/**
 * Failure response
 */
function failure (error: string, message: string) {
  return {
    isValid: false,
    error: {
      name: `LUHN_${error}`,
      message
    }
  }
}

/**
* Making sure the given value has the correct minimum length.
*/
const ensureLength = (value: string, { min, max }: Options): Response => {
  if (min && value.length < min) {
    return failure(Errors.TOO_SHORT, 'The Luhn number is too short.')
  }

  if (max && value.length > max) {
    return failure(Errors.TOO_LONG, 'The Luhn number is too long.')
  }

  return {
    isValid: true
  }
}

/**
* Ensure the checksum of the Luhn is correct.
*/
const ensureChecksum = (value: string) => {
  const sum = value
    .split('')
    .reverse()
    .map(Number)
    .map((x, i) => i % 2 ? x * 2 : x)
    .map((x) => x > 9 ? x - 9 : x)
    .reduce((x, y) => x + y)

  return sum % 10 === 0
}

/**
* Validate Luhn.
*/
export function luhn (value: string | number, { min, max}: Options): Response {
  const cleaned = value.toString().replace(/[+-]/g, '').replace(/\s+/g, '')
  const length = ensureLength(cleaned, { min, max })

  if (!length.isValid) {
    return length
  }

  const onlyNumbers = /^\d+$/
  if (!onlyNumbers.test(cleaned)) {
    return failure(Errors.INVALID_CHARACTERS, 'The Luhn number contains invalid characters.')
  }

  if (!ensureChecksum(cleaned)) {
    return failure(Errors.INVALID_CHECKSUM, 'The Luhn number has an invalid checksum.')
  }

  return {
    isValid: true
  }
}
