import validateLuhn from './rules/luhn-rule'
import validateSsn from './rules/ssn-rule'
import { validateBankgiro, validatePlusgiro, validateBankAccount } from './rules/bank-rules'

export {
  validateLuhn,
  validateSsn,
  validateBankgiro,
  validatePlusgiro,
  validateBankAccount
}
