// eslint-disable-next-line no-unused-vars
import { luhn } from '../utils/luhn'
import Kontonummer from 'kontonummer'

export function validateBankgiro (value: string | number): boolean {
  return luhn(value, { min: 7, max: 8 }).isValid
}

export function validatePlusgiro (value: string | number): boolean {
  return luhn(value, { min: 2, max: 8 }).isValid
}

export function validateBankAccount (value: string | number): boolean {
  try {
    new Kontonummer(value)
  } catch (e) {
    return false
  }

  return true
}
