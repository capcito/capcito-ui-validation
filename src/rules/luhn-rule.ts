import { luhn } from '../utils/luhn'

/**
 * Global pattern to check against.
 */
const pattern = /^\d{6}(\d{2})?[+-]?\d{4}$/

export default (value: string | number) => {
  const cleaned = value.toString().replace(/[+-]/, '').slice(-10)

  return luhn(value, { min: 10 }).isValid &&
    pattern.test(value.toString())
}
