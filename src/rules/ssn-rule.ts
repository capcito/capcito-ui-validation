import { luhn } from '../utils/luhn'

/**
 * Ensure the given value is a valid date.
 */
const ensureValidDate = (value: string) => {
  const match = /^(\d{2})(\d{2})(\d{2})/.exec(value)

  if (!match?.length) {
    return false
  }

  const yearStr = match[1]
  const year = Number(yearStr)
  const month = Number(match[2]) - 1
  let day = Number(match[3])

  if (day > 60) {
    day -= 60
  }

  const date = new Date(year, month, day)

  const yearIsValid = String(date.getFullYear()).substr(-2) === yearStr
  const monthIsValid = date.getMonth() === month
  const dayIsValid = date.getDate() === day

  return yearIsValid && monthIsValid && dayIsValid
}

/**
 * Main validation method.
 */
export default (value: string) => {
  /**
   * Input: YYYYMMDD(+ or -)XXXX
   * Output: YYMMDDXXXX
   */
  const cleaned = value.replace(/[+-]/, '').slice(-10)

  return ensureValidDate(cleaned) && luhn(cleaned, { min: 10 }).isValid
}
