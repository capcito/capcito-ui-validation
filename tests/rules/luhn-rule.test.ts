import { validateLuhn } from '../../src/index'

it('validateLuhn will validate thruthly with legit numbers of different formats', () => {
  expect(validateLuhn('559028-3601')).toEqual(true)
  expect(validateLuhn(5590283601)).toEqual(true)
  expect(validateLuhn('559028-3601')).toEqual(true)
})

test('validateLuhn will validate thruthly with a dash as a seperator', () => {
  expect(validateLuhn('559028-3601')).toBe(true)
})

test('validateLuhn will validate thruthly with a plus as a seperator', () => {
  expect(validateLuhn('559028+3601')).toBe(true)
})

test('validateLuhn will validate false with wrong formats', () => {
  expect(validateLuhn('55902-83601')).toEqual(false)
  expect(validateLuhn('559028 3601')).toEqual(false)
  expect(validateLuhn('55 90 28 3601')).toEqual(false)
  expect(validateLuhn('55 902836-01')).toEqual(false)
})

test('validateLuhn will validate thruthly with no seperator', () => {
  expect(validateLuhn('5590283601')).toBe(true)
})

test('validateLuhn will validate falsy with other char as seperator', () => {
  expect(validateLuhn('559028@3601')).toBe(false)
})

test('validateLuhn will validate falsy with invalid check character', () => {
  expect(validateLuhn('559028-0000')).toBe(false)
})

test('validateLuhn will validate falsy with invalid length', () => {
  expect(validateLuhn('559028')).toBe(false)
})
