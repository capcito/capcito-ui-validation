import { validateBankgiro } from '../../../src/index'

describe('Validate Bankgiro', () => {
  it('should return isValid on legit numbers of different formats', () => {
    expect(validateBankgiro('5402-9681')).toEqual(true)
    expect(validateBankgiro(54029681)).toEqual(true)
    expect(validateBankgiro('54029681')).toEqual(true)
    expect(validateBankgiro('297-3675')).toEqual(true)
    expect(validateBankgiro('640-5070')).toEqual(true)
  })

  it('should fail on too short numbers', () => {
    expect(validateBankgiro('1')).toEqual(false)
  })

  it('should fail on too long numbers', () => {
    expect(validateBankgiro('5402-96810')).toEqual(false)
    expect(validateBankgiro('540296810')).toEqual(false)
  })

  it('should fail on invalid checksum', () => {
    expect(validateBankgiro('5402-9682')).toEqual(false)
    expect(validateBankgiro('54029682')).toEqual(false)
  })

  it('should fail on invalid characters', () => {
    expect(validateBankgiro('5402-968X')).toEqual(false)
  })
})
