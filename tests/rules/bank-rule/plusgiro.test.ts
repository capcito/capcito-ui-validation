import { validatePlusgiro } from '../../../src/index'

describe('Validate Plusgiro', () => {
  it('should return isValid on legit numbers of different formats', () => {
    expect(validatePlusgiro('28 65 43-4')).toEqual(true)
    expect(validatePlusgiro(2865434)).toEqual(true)
    expect(validatePlusgiro('2865434')).toEqual(true)
    expect(validatePlusgiro('2865-434')).toEqual(true)
    expect(validatePlusgiro('2865 434')).toEqual(true)
    expect(validatePlusgiro('410 54 68-5')).toEqual(true)
    expect(validatePlusgiro('90 20 03-3')).toEqual(true)
    expect(validatePlusgiro('4-2')).toEqual(true)
    expect(validatePlusgiro('4-2')).toEqual(true)
  })

  it('should fail on too short numbers', () => {
    expect(validatePlusgiro('1')).toEqual(false)
  })

  it('should fail on too long numbers', () => {
    expect(validatePlusgiro('123456789')).toEqual(false)
  })

  it('should fail on invalid checksum', () => {
    expect(validatePlusgiro('410 54 68-6')).toEqual(false)
  })

  it('should fail on invalid characters', () => {
    expect(validatePlusgiro('capcito')).toEqual(false)
  })
})
