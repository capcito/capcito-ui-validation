import { validateBankAccount } from '../../../src/index'

describe('Bank account', () => {
  it('Should validate a Handelsbanken account', () => {
    expect(validateBankAccount('6789123456789')).toEqual(true)
  })

  it('Should verify a Swedbank account number with a 5 digit sorting code', () => {
    expect(validateBankAccount('8424-4,983 189 224-6')).toEqual(true)
  })

  it('Should veify an account number from Sparbanken Tanum', () => {
    expect(validateBankAccount('8351-9,392 242 224-5')).toEqual(true)
  })

  it('Should verify an account number from Sparbank i Hudiksvall', () => {
    expect(validateBankAccount('8129-9,043 386 711-6')).toEqual(true)
  })

  it('Should verify a Nordea personkonto', () => {
    expect(validateBankAccount('3300 000620-5124')).toEqual(true)
  })

  it('Should verify a klarna account number', () => {
    expect(validateBankAccount('97891111113')).toEqual(true)
  })

  it('Should throw for an invalid account number', () => {
    expect(validateBankAccount('123456789')).toEqual(false)
  })

  it('Should throw if the check digit is invalid', () => {
    expect(validateBankAccount('6789123456788')).toEqual(false)
  })

  it('Should throw if the check digit on a 5 digit sorting code is invalid', () => {
    // Same account number as 'swedbank' above, but different clearing number
    expect(validateBankAccount('8424-1,983 189 224-6')).toEqual(false)
  })
})
