import { validateSsn } from '../../src/index'

test('validateSsn will validate thruthly with a dash as a seperator', () => {
  expect(validateSsn('951201-6065')).toBe(true)
})

test('validateSsn will validate thruthly with a plus as a seperator', () => {
  expect(validateSsn('951201+6065')).toBe(true)
})

test('validateSsn will validate thruthly with no seperator', () => {
  expect(validateSsn('9512016065')).toBe(true)
})

test('validateSsn will validate falsy with other char as seperator', () => {
  expect(validateSsn('951201@6065')).toBe(false)
})

test('validateSsn will validate falsy with wrong formats', () => {
  expect(validateSsn('951201 6065')).toBe(false)
  expect(validateSsn('95 12 01 6065')).toBe(false)
})

test('validateSsn will validate falsy with an invalid date', () => {
  const nextYear = (new Date()).getFullYear().toString().substr(2, 2) + 1

  expect(validateSsn(`${nextYear}1201-6065`)).toBe(false)
  expect(validateSsn('951301-6065')).toBe(false)
  expect(validateSsn('951399-6065')).toBe(false)
})

test('validateSsn will validate falsy with invalid check character', () => {
  expect(validateSsn('951201-6060')).toBe(false)
})

test('validateSsn will validate falsy with invalid length', () => {
  expect(validateSsn('951201')).toBe(false)
})
